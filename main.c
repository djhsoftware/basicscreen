#include <hardware/cia.h>
#include <graphics/gfxbase.h>
#include <graphics/view.h>
#include <clib/exec_protos.h>
#include <clib/graphics_protos.h>
#include <stdio.h>
#include <stdlib.h>

#include "lib/iff.h"
#include <proto/iff.h>

#define WIDTH 320
#define HEIGHT 256

struct View view;
struct ViewPort viewport;

struct ColorMap *GetColorMap();
struct GfxBase *GfxBase;
struct IFFBase *IFFBase;
extern struct CIA ciaa, ciab;
struct View *oldview;

int rasScrollDir1 = 1;
int rasScrollDir2 = 2;

int FreeMemory();
void Scroll(void);
int MouseClick(void);

struct BitmapData
{
	// Color data
	struct ColorMap* cm;
	UWORD colortable[32];
	LONG colorTableSize;
	
	// The bitmap data
	UBYTE planes;
	struct BitMap *bitmap;
	
	struct RasInfo rasInfo;
};

struct BitmapData *LoadBitmap(char *filename);

int main(int argc, char **argv)
{
	GfxBase = (struct GfxBase *) OpenLibrary("graphics.library", 0);
	if (GfxBase == NULL) exit(1);

	IFFBase = (struct IFFBase *)OpenLibrary("iff.library", 0);
	if (IFFBase == NULL) exit(1);
	
	InitView(&view);
	view.ViewPort = &viewport;
	
	InitVPort(&viewport);
	viewport.DWidth = WIDTH;
	viewport.DHeight = HEIGHT;
	viewport.Modes = 0;

	printf("InitBitMap 1\n");
	struct BitmapData *bd1 = LoadBitmap("res/gfx/background.iff");
	viewport.RasInfo = &bd1->rasInfo;

	printf("GetColorMap\n");	
	viewport.ColorMap = bd1->cm;	
	
	// Load the color table
	LoadRGB4(&viewport, bd1->colortable, bd1->colorTableSize);
	
	printf("MakeVPort\n");
	MakeVPort(&view, &viewport);

	printf("MrgCop\n");
	MrgCop(&view);

	printf("LoadView");
	oldview = GfxBase->ActiView;
	LoadView(&view);
	
	printf("Wait\n");
    while (!MouseClick())
    {
		WaitTOF();
    }
	
	printf("Unload");
	LoadView(oldview);
	WaitTOF();
	FreeMemory();
	
	CloseLibrary((struct Library *) GfxBase);
	CloseLibrary((struct Library *) IFFBase);

	return 0;
}

int FreeMemory()
{
  FreeVPortCopLists(&viewport);
  FreeCprList(view.LOFCprList);
  return 0;
}

int MouseClick(void)
{
    return !(ciaa.ciapra & CIAF_GAMEPORT0);
}

struct BitmapData *LoadBitmap(char *filename)
{	
	struct BitmapData *bitmapData = AllocMem(sizeof(struct BitmapData), MEMF_CHIP | MEMF_CLEAR);
	
	// Load background into bitmap
	printf("Load bitmap: %s\n",filename);
	IFFL_HANDLE iff = IFFL_OpenIFF(filename, IFFL_MODE_READ);
	if(iff == NULL)
	{
        printf("IFF open failed :(\n");
        return FALSE;
	}		

	struct IFFL_BMHD *bmhd = IFFL_GetBMHD(iff);
	if(bmhd==NULL)
	{
		printf("IFF is not bitmap :(\n");
        return FALSE;
	}
	
	bitmapData->cm = GetColorMap(16);
	if (bitmapData->cm == NULL)
	{
		printf("Could not get ColorMap\n");
        return FALSE;
	}
	
	// Load the background colour map
	bitmapData->colorTableSize = IFFL_GetColorTab(iff, bitmapData->colortable);	
	
	// Allocate the bitmap and decode it
	bitmapData->planes = bmhd->nPlanes;
	printf("Btmap: %dx%d @ %d planes\n",bmhd->w, bmhd->h, bmhd->nPlanes);
	bitmapData->bitmap = AllocBitMap(bmhd->w, bmhd->h, bmhd->nPlanes, BMF_CLEAR | BMF_DISPLAYABLE, NULL);
	if(!IFFL_DecodePic(iff, bitmapData->bitmap))
	{
		printf("Could not decode map bitmap\n");
        return FALSE;
	}
	
	bitmapData->rasInfo.BitMap = bitmapData->bitmap;
	bitmapData->rasInfo.RxOffset = 0;
	bitmapData->rasInfo.RyOffset = 0;
	
	return bitmapData;
}